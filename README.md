<h1>uv.js</h1>

<img align="centre" src="logo.jpg">

<p>JavaScript - блиотека, позволяющая генерировать и манипулировать содержимым веб-приложения, без использования какой-либо верстки.
Все элементы DOM-древа генерируются напрямую самой библиотекой, без надобности прибегать к верстке или услугам верстальщиков.</p>

<h1>Документация</h1>

<ul>
	<li><a href="#start">Start</a></li>
	<li><a href="#createElem">.createElem()</a></li>
	<li><a href="#removeElem">.removeElem()</a></li>
	<li><a href="#addTag">.addTag()</a></li>
	<li><a href="#setAttr">.setAttr()</a></li>
	<li><a href="#removeAttr">.removeAttr()</a></li>
	<li><a href="#connectStyle">.connectStyle()</a></li>
	<li><a href="#connectScript">.connectScript()</a></li>
	<li><a href="#createList">.createList()</a></li>
	<li><a href="#addStorage">.addStorage()</a></li>
	<li><a href="#getStorage">.getStorage()</a></li>
	<li><a href="#viewStorage">.viewStorage()</a></li>
	<li><a href="#setStyle">.setStyle()</a></li>
	<li><a href="#page">.page()</a></li>
</ul>

<h2 id="start">Start</h2>
<p>Перед началом определите функцию Uv(setting)</p>
<code>var obj = new Uv({
	clearDocument: true,
	metaKeywords: true,
		keywords: 'home Home',
	metaCharset: 'utf-8',
	fileName: 'script.js',
	animateCss: true
});</code>
<p>Аргумент setting принимает объект с начальными настройками.</p>
<ul>
	<li><b>clearDocement(boolean)</b> - определяет, нужно-ли очищать документ после загрузки.</li>
	<li><b>metaKeywords(boolean)</b> - определяет ключевые слова страницы.</li>
	<li><b>keywords(string)</b> - ключевые слова через пробел. Указывать только если metaKeywords true.</li>
	<li><b>metaCharset(string)</b> - определяет кодировку страницы. По умолчанию utf-8.</li>
	<li><b>fileName(string)</b> - имя пользовательского скрипта.</li>
	<li><b>animateCss(boolean)</b> - определяет, будет-ли подключена библиотека animate.css</li>
</ul>

<h2 id="createElem">.createElem(elem, node, idElem, select)</h2>
<p>Создаёт элемент на странице</p>
<ul>
	<li><b>elem</b> - создоваемый элемент.</li>
	<li><b>node</b> - содержимое(текст) элемента.</li>
	<li><b>idElem</b> - id создоваемого элемента.</li>
	<li><b>select</b> - где будет создан элемент(указывать по селектору).</li>
</ul>
<p>Пример: <br><code>obj.createElem('button', 'this is button', 'btn1', '#block');</code></p>

<h2 id="removeElem">.removeElem(block, select)</h2>
<p>Удаляет элемент на странице</p>
<ul>
	<li><b>block</b> - блок в котором находится элемент(указывать по селектору).</li>
	<li><b>node</b> - удаляемый элемент.</li>
</ul>
<p>Пример: <br><code>obj.removeElem('body > span', '#btn1');</code></p>

<h2 id="addTag">.addTag(tag)</h2>
<p>В случае отсутствия добовляет тег в общий список</p>
<ul>
	<li><b>tag</b> - добовляемый тег.</li>
</ul>
<p>Пример: <br><code>obj.addTag('my-tag');</code></p>

<h2 id="setAttr">.setAttr(attr, valAttr, select)</h2>
<p>Устанавливает атрибут у элемента</p>
<ul>
	<li><b>attr</b> - добовляемый атрибут.</li>
	<li><b>valAttr</b> - значение атрибута.</li>
	<li><b>select</b> - элемент к которому будет добавлен атрибут.</li>
</ul>
<p>Пример: <br><code>obj.setAttr('class', 'menu', '.header');</code></p>

<h2 id="removeAttr">.removeAttr(attr, select)</h2>
<p>Удаляет атрмбут у элемента</p>
<ul>
	<li><b>attr</b> - удаляемый атрибут.</li>
	<li><b>select</b> - элемент у которого будет удалён атрибут.</li>
</ul>
<p>Пример: <br><code>obj.removeAttr('alt', '#images');</code></p>

<h2 id="connectStyle">.connectStyle(src)</h2>
<p>Подключает стили к документу</p>
<ul>
	<li><b>src</b> - путь к стилям.</li>
</ul>
<p>Пример: <br><code>obj.connectStyle('https://litter-css.ru/download/litter-css/styles/litter.css');</code></p>
<p><strong>Примечание !</strong> можно подключать как локальные, так и удалённые стили.</p>

<h2 id="connectScript">.connectScript(src)</h2>
<p>Подключает скрипт к документу</p>
<ul>
	<li><b>src</b> - путь к скрипту.</li>
</ul>
<p>Пример: <br><code>obj.connectScript('js/script.js');</code></p>
<p><strong>Примечание !</strong> можно подключать как локальные, так и удалённые стили.</p>

<h2 id="createList">.connectList(typeList, elements, idList, select)</h2>
<p>Создаёт список на странице</p>
<ul>
	<li><b>typeList</b> - тип списка(ul, ol).</li>
	<li><b>elements</b> - массив с элементами списка.</li>
	<li><b>idList</b> - id данного списка.</li>
	<li><b>select</b> - блок в котором будут создан список.</li>
</ul>
<p>Пример: <br><code>obj.createList('ol', ['lol', 'kek'], 'body');</code></p>

<h2 id="addStorage">.addStorage(name, value)</h2>
<p>Создаёт запись в localStorage</p>
<ul>
	<li><b>name</b> - имя новой записи.</li>
	<li><b>value</b> - содержимое записи.</li>
</ul>
<p>Пример: <br><code>obj.addStorage('text', 'simple text');</code></p>

<h2 id="getStorage">.getStorage(name)</h2>
<p>Получает запись из localStorage</p>
<ul>
	<li><b>name</b> - имя записи.</li>
</ul>
<p>Пример: <br><code>alert(obj.getStorage('text'));</code></p>

<h2 id="viewStorage">.viewStorage(view)</h2>
<p>Выводит все данные localStorage</p>
<ul>
	<li><b>view</b> - тип вывода. <i>console</i> - выводит данные в консоль. <i>table</i> - выводит данные таблицой на странице.</li>
</ul>
<p>Пример: <br><code>obj.viewStorage('table');</code></p>

<h2 id="setStyle">.setStyle(select, style)</h2>
<p>Присваивает стили элементу</p>
<ul>
	<li><b>select</b> - блок которому будут присвоены стили.</li>
	<li><b>style</b> - сами стили.</li>
</ul>
<p>Пример: <br><code>obj.setStyle('#header', 'padding: 10px;');</code></p>

<h2 id="page">.page(clearDoc, callback)</h2>
<p>Обновляет содержимое страницы.</p>
<ul>
	<li><b>clearDoc</b> - определяет будет ли очищена страница.</li>
	<li><b>callback</b> - callback функция в которой должна рендериться страница.</li>
</ul>
<p>Пример: <br><code><pre>
obj.page(true, function(i) {
	obj.createElem('p', text_page2, 'text2', 'body'); //create paragraph			obj.createElem('button', 'back page', 'btn2', 'body'); // create button		
});
</pre></code></p>


