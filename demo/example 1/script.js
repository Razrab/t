var obj = new Uv({
	clearDocument: true,
	metaKeywords: false,
	metaCharset: 'utf-8',
	fileName: 'script.js'
	animateCss: true
});

obj.createElem('h1', 'Hello, world !', 'h1', 'body');
obj.createElem('h2', 'Hello, world !', 'h2', 'body');
obj.createElem('h3', 'Hello, world !', 'h3', 'body');
obj.createElem('h4', 'Hello, world !', 'h4', 'body');
obj.createElem('h5', 'Hello, world !', 'h5', 'body');
obj.createElem('h6', 'Hello, world !', 'h6', 'body');