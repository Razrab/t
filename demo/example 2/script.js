var obj = new Uv({
	clearDocument: true,
	metaKeywords: false,
	metaCharset: 'utf-8',
	fileName: 'script.js'
	animateCss: true
});

obj.createElem('p', 'simple text', 'text1', 'body');

obj.createElem('textarea', '', 'text2', 'body');
obj.setAttr('cols', '50', '#text2');
obj.setAttr('rows', '10', '#text2');
obj.setAttr('onkeydown', 'func1()', '#text2');

var text1 = document.querySelector('#text1');
var text2 = document.querySelector('#text2');

function func1() {
	text1.innerHTML = text2.value;
}
