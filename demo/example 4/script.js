var obj = new Uv({
	clearDocument: true,
	metaKeywords: false,
	metaCharset: 'utf-8',
	fileName: 'script.js'
	animateCss: true
});

var text_page1 = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio perferendis reprehenderit saepe mollitia necessitatibus qui, iusto voluptas, ipsa laboriosam quisquam.';
var text_page2 = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio perferendis reprehenderit saepe mollitia necessitatibus qui, iusto voluptas, ipsa laboriosam quisquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita natus recusandae autem porro necessitatibus dolores iusto distinctio repellat amet est.';

	a();
	function a() {
		obj.page(true, function(i) {
			obj.createElem('p', text_page1, 'text1', 'body'); //create paragraph
			obj.createElem('button', 'next page', 'btn1', 'body');// create button
			obj.setAttr('onclick', 'b();', '#btn1');
		});
	}

	function b() {
		obj.page(true, function(i) {
			obj.createElem('p', text_page2, 'text2', 'body'); //create paragraph
			obj.createElem('button', 'back page', 'btn2', 'body'); // create button
			obj.setAttr('onclick', 'a();', '#btn2');
		});
	}
