var tag = ['p', 'button', 'div', 'span', 'pre', 'code', 'i', 'b', 'big', 'small',
	'a', 'abbr', 'addres', 'area', 'article', 'aside', 'audio', 'basefont', 'blockquote',
	'canvas', 'caption', 'center', 'cite', 'col', 'colgroup', 'dd', 'del', 'dfn', 'dl',
	'dt', 'em', 'embed', 'figure', 'font', 'footer', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
	'iframe', 'img', 'ins', 'input', 'label', 'li', 'main', 'map', 'menu', 'nav', 'nobr',
	'noscript', 'object', 'ol', 'optgroup', 'option', 'script', 'select', 'span',
	'strong', 'sub', 'sup', 'tabel', 'tbody', 'textarea', 'td', 'th', 'time', 'tr',
	'track', 'tt', 'u', 'ul', 'video', 'wbr', 'datalist', 'detalis', 'keygen',
	'meter', 'progress', 'rp', 'ruby', 'source', 'form'
];

function Uv(setting) {
	/*
	setting - объект в котором хранятся начальные настройки.
		clearDocument(true | false) - определяет будет-ли очищена страница в начале загрузки
		fileName - имя файла с пользовательским скриптом
		metaKeywords(true | false) - добовляет ключевые слова
			keywords - ключевые слова. Если metaKeywords true, то создаём тег. Слдова указывать через пробел
		metaCharset - определяет кодировку файла. Если значение пустое по умолчанию кодировка UTF-8	
	*/

	//очистка документа
	if (setting.clearDocument == true) {
		document.body.innerHTML = '';
	}

	//ключевые слова
	if (setting.metaKeywords == true) {
		var e = document.createElement('meta');
		if (!setting.keywords) {
			console.error('\'keywords\' not found ')
		} else {
			var k = setting.keywords.split(' ');
			e.name = 'keywords';
			e.content = k.join(', ')

			document.head.appendChild(e);
		}
	}

	//кодировка документа
	if (setting.metaCharset == '' | !setting.metaCharset) {
		var e = document.createElement('meta');
		e.setAttribute('charset', 'utf-8');

		document.head.appendChild(e);
	} else {
		var e = document.createElement('meta');
		e.setAttribute('charset', setting.metaCharset);

		document.head.appendChild(e);
	}

	if (setting.animateCss == true) {
		var e = document.createElement('link');

		e.type = 'text/css';
		e.rel = 'stylesheet';
		e.href = 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css';

		document.head.appendChild(e);
		console.info('Include animate')
	}

	this.createElem = function(elem, node, idElem, select) {
		/*
			elem -  элемент
			node - текс внутри элемента
			select -  в чём будет создаваться элемент
			idElem -  id элемента
		*/

		if (tag.indexOf(elem) == -1) {
			//тега на существование
			console.error('Element not found ! Suggested to use a addTag() for added element.');
		} else {
			if (select == '') {
				//если block пуст, то создаём в body
				var b = document.body;
			} else {
				if (!document.querySelector(select)) {
					var b = document.body;
				} else {
					var b = document.querySelector(select);
				}
			}
			var e = document.createElement(elem);
			var n = document.createTextNode(node);
			e.appendChild(n);
			e.id = idElem;

			b.appendChild(e);
		}
	}

	this.addTag = function(newTag) {
		tag.push(newTag);
	}

	this.setAttr = function(attr, valAttr, select) {
		/*
			attr - добовляемый атрибут
			valAttr - значение атрибута
			select - id элемента которму будет пресвоен атрибут
		*/
		var e = document.querySelector(select);

		if (attr == '') {
			console.error('The attr value is empty');
		} else {
			try {
				e.setAttribute(attr, valAttr);
			} catch (err) {
				console.error('Set attribute error');
			}
		}
	}

	this.removeAttr = function(attr, select) {
		var e = document.querySelector(select);

		if (attr == '') {
			console.error('Attribute value not found');
		} else {
			if (select == '') {
				console.error('Element not found');
			} else {
				try {
					e.removeAttribute(attr);
				} catch (err) {
					console.error('Remove attribute error');
				}
			}
		}
	}

	this.removeElem = function(block, select) {
		/*
			block - в чём находится элемент
			select - id элемента
		*/
		var e = document.querySelector(select);
		var b = document.querySelector(block);
		b.removeChild(e);
	}

	this.connectStyle = function(src) {
		try {
			var e = document.createElement('link');

			e.type = 'text/css';
			e.rel = 'stylesheet';
			e.href = src;

			document.head.appendChild(e);
		} catch (err) {
			console.error('connect error: ' + err.name)
		}
	}

	this.connectScript = function(src) {
		try {
			if (src == setting.fileName) {
				console.error('wrong intoduced: fileName')
			} else {
				var e = document.createElement('script');

				e.type = 'text/javascript';
				e.src = src;
			}
			document.body.appendChild(e);
		} catch (err) {
			console.error('connect error: ' + err.name)
		}
	}

	this.createList = function(typeList, elements, idList, select) {
		/*
			typeList - пределяет тип списка (ul, ol)
			elements - массив с значениями пунктов
			select - в чём будет создоваться список
		*/
		if (select == '') {
			var b = document.body;
		} else {
			var b = document.querySelector(select);
		}

		var tl = document.createElement(typeList);
		tl.id = idList;
		b.appendChild(tl);

		for (var i = 0; i <= elements.length - 1; i++) {
			var li = document.createElement('li');
			var text = document.createTextNode(elements[i]);

			li.appendChild(text);
			tl.appendChild(li);
		}
	}

	this.addStorage = function(name, value) {
		if (name == '') {
			console.error('Name empty');
		} else {
			localStorage.setItem(name, value);
		}
	}

	this.getStorage = function(name) {
		if (name == '') {
			console.error('Name empty');
		} else {
			localStorage.getItem(name);
		}
	}

	this.removeStorage = function(name) {
		if (name == '') {
			console.error('Name empty');
		} else {
			localStorage.setItem(name);
		}
	}

	this.viewStorage = function(view) {
		switch (view) {
			case 'table':
				var t = document.createElement('table');
				t.id = 'tlocal217'
				document.body.appendChild(t);

				var s = localStorage;
				for (var i = 0; i < s.length; i++) {
					var k = s.key(i);
					console.log(k + '=' + s.getItem(k));
					var tab = document.querySelector('#tlocal21');
					var tr = document.createElement('tr');
					t.appendChild(tr);

					var th = document.createElement('th');
					var t1 = document.createTextNode(k);
					th.appendChild(t1);
					tr.appendChild(th);

					var th2 = document.createElement('th')
					var t2 = document.createTextNode(s.getItem(k));
					th2.appendChild(t2);
					tr.appendChild(th2);

					th.style.border = '1px solid #555';
					th2.style.border = '1px solid #555';
				}
				break;
			case 'console':
				var s = localStorage;
				for (var i = 0; i < s.length; i++) {
					var k = s.key(i);
					console.log(k + '=' + s.getItem(k));
				}
				break;
			case 'modal':

				break;
		}
	}

	this.setStyle = function (select, style) {
		var e = document.querySelector(select);
		e.style = Elemstyle;
	}

	this.page = function (clear, callback) {
		if (clear == true) {
			document.body.innerHTML = '';
		}
		callback(1)
	}
}
	